- Add cookies to `cookies.toml` (copy from template `cookies.example.toml`)
- Edit some things in the `genshin_daily_login.service` file:
  - script's working directory
  - user to run script as
- Copy the `systemd` service script to `/etc/systemd/system`
- Enable the service with `systemctl`
