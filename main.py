# Adapted from https://github.com/darkGrimoire/hoyolab-daily-bot
# Script logs in and sleeps for 24h

from datetime import datetime, timedelta
import sys
import os
import requests
import time
import toml
import random
import json

ACT_ID = 'e202102251931481'
DOMAIN_NAME = '.mihoyo.com'

log = None

def getDailyStatus(cookies):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Origin': 'https://webstatic-sea.mihoyo.com',
        'Connection': 'keep-alive',
        'Referer': f'https://webstatic-sea.mihoyo.com/ys/event/signin-sea/index.html?act_id={ACT_ID}&lang=en-us',
        'Cache-Control': 'max-age=0',
    }

    params = (
        ('lang', 'en-us'),
        ('act_id', ACT_ID),
    )

    try:
        response = requests.get('https://hk4e-api-os.mihoyo.com/event/sol/info', headers=headers, params=params, cookies=cookies)
        return response.json()
    except requests.exceptions.ConnectionError as e:
        log.write('CONNECTION ERROR: cannot get daily check-in status\n')
        log.write(repr(e) + '\n')
        return None
    except Exception as e:
        log.write('UNKNOWN ERROR:\n')
        log.write(repr(e) + '\n')
        return None

def isClaimed(cookies):
    resp = getDailyStatus(cookies)
    if resp:
        return resp['data']['is_sign']
    else:
        return None

def claimReward(cookies):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        'Content-Type': 'application/json;charset=utf-8',
        'Origin': 'https://webstatic-sea.mihoyo.com',
        'Connection': 'keep-alive',
        'Referer': f'https://webstatic-sea.mihoyo.com/ys/event/signin-sea/index.html?act_id={ACT_ID}&lang=en-us',
    }

    params = (
        ('lang', 'en-us'),
    )

    data = { 'act_id':ACT_ID }

    try:
        response = requests.post('https://hk4e-api-os.mihoyo.com/event/sol/sign', headers=headers, params=params, cookies=cookies, data=json.dumps(data))
        return response.json()
    except requests.exceptions.ConnectionError as e:
        log.write('CONNECTION ERROR: cannot claim daily check-in reward\n')
        log.write(repr(e) + '\n')
        return None
    except Exception as e:
        log.write('UNKNOWN ERROR:\n')
        log.write(repr(e) + '\n')
        return None

def bake_cookies():
    with open('./cookies.toml', 'r') as cookie_toml:
        return toml.loads(''.join(cookie_toml.readlines()))

def main():
    cookies_for_each_player = bake_cookies()

    while True:
        global log
        with open('./mhy_daily_claim.log', 'a+') as log:
            for player in cookies_for_each_player:
                log.write(f'\nGot cookies for player {player}.\n')
                player_cookies = cookies_for_each_player.get(player)

                claimed_today = False
                while not claimed_today:
                    claimed = isClaimed(player_cookies)
                    if claimed is not None:
                        if not claimed:
                            log.write(f'Reward not claimed yet. Claiming reward...\n')

                            resp = claimReward(player_cookies)
                            if resp:
                                log.write(f'Reward claimed at {datetime.now().strftime("%d %B, %Y | %H:%M:%S")}: {resp["message"]}\n')
                                claimed_today = True

                        else:
                            log.write(f'Reward already claimed when checked at {datetime.now().strftime("%d %B, %Y | %H:%M:%S")}\n')
                            claimed_today = True

                    else:
                        log.write(f'Error at {datetime.now().strftime("%d %B, %Y | %H:%M:%S")}, retrying...\n')
                        time.sleep(60)

        # each player logged in, let's sleep for the day
        # uniform distribution so this shouldn't drift too much
        time.sleep(60*60*24+random.randint(-60,60))

if __name__ == "__main__":
    main()